package com.vojlrove.testjson.network;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class NetworkModule {
    private final String mBaseUrl;

    public NetworkModule(String url) {
        this.mBaseUrl = url;
    }

    @Provides
    @Singleton
    @Inject
    APIManager provideApiManager() {
        List<Interceptor> interceptors = new ArrayList<>();
        interceptors.add(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        return new APIManager(this.mBaseUrl, interceptors);
    }
}