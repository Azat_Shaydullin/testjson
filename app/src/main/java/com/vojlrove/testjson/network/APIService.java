package com.vojlrove.testjson.network;

import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.model.User;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface APIService {

    @GET("users")
    Single<List<User>> getUsers();

    @GET("posts")
    Single<List<Post>> getPosts();

    @GET("comments")
    Single<List<Comment>> getComments();
}