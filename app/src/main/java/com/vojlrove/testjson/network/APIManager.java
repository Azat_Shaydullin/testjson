package com.vojlrove.testjson.network;

import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.model.User;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIManager {

    private APIService mApiService;

    public APIManager(String baseUrl, List<Interceptor> interceptors) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS);

        for (Interceptor interceptor : interceptors) {
            httpClient.addInterceptor(interceptor);
        }

        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        });

        OkHttpClient client = httpClient.build();
        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
        mApiService = mRetrofit.create(APIService.class);
    }

    public Single<List<User>> getUsers() {
        return mApiService.getUsers();
    }

    public Single<List<Post>> getPosts() {
        return mApiService.getPosts();
    }

    public Single<List<Comment>> getComments() {
        return mApiService.getComments();
    }
}