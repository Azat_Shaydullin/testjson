package com.vojlrove.testjson.db;

import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.model.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Dictionaries {
    private List<User> mUsers;
    private List<Post> mPosts;
    private List<Comment> mComments;

    @Inject
    public Dictionaries() {
    }

    public void setUsers(List<User> users) {
        mUsers = users;
    }

    public List<User> getUsers() {
        return mUsers;
    }

    public void setPosts(List<Post> posts) {
        mPosts = posts;
    }

    public List<Post> getPosts(int userId) {
        List<Post> ret = new ArrayList<>();
        for (Post post : mPosts) {
            if (post.getUserId() == userId) {
                ret.add(post);
            }
        }
        return ret;
    }

    public Post getPost(int postId) {
        for (Post post : mPosts) {
            if (post.getId() == postId) {
                return post;
            }
        }
        return null;
    }

    public void addPost(Post post) {
        mPosts.add(post);
    }

    public void removePost(int postId) {
        Iterator<Post> iterator = mPosts.iterator();
        while(iterator.hasNext()) {
            Post item = iterator.next();
            if (item.getId() == postId) {
                iterator.remove();
                break;
            }
        }
    }

    public void setComments(List<Comment> comments) {
        mComments = comments;
    }

    public List<Comment> getComments(int postId) {
        List<Comment> ret = new ArrayList<>();
        for (Comment comment : mComments) {
            if (comment.getPostId() == postId) {
                ret.add(comment);
            }
        }
        return ret;
    }
}