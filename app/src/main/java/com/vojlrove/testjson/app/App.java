package com.vojlrove.testjson.app;

import androidx.multidex.MultiDexApplication;

import com.vojlrove.testjson.Constants;
import com.vojlrove.testjson.network.NetworkModule;

public class App extends MultiDexApplication {
    private static App sInstance;
    private static AppComponent sAppComponent;

    public static App get() {
        return sInstance;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        sAppComponent = buildComponent();
        sAppComponent.inject(this);
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent
                .builder()
                .networkModule(new NetworkModule(getUrl()))
                .build();
    }

    public String getUrl() {
        return Constants.API_ROOT;
    }
}
