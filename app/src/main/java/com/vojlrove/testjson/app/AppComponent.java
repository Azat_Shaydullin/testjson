package com.vojlrove.testjson.app;

import com.vojlrove.testjson.network.NetworkModule;
import com.vojlrove.testjson.ui.posts.PostsPresenter;
import com.vojlrove.testjson.ui.users.UsersPresenter;
import com.vojlrove.testjson.ui.comments.CommentsPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                NetworkModule.class
        }
)

public interface AppComponent {

    void inject(App app);

    void inject(UsersPresenter usersPresenter);

    void inject(PostsPresenter postsPresenter);

    void inject(CommentsPresenter commentsPresenter);
}