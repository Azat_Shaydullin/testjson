package com.vojlrove.testjson.ui.posts;

import com.arellomobile.mvp.InjectViewState;
import com.vojlrove.testjson.app.App;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.ui.BasePresenter;

import javax.inject.Inject;

@InjectViewState
public class PostsPresenter extends BasePresenter<IPosts> {

    @Inject
    PostsModel mModel;

    PostsPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void attachView(IPosts view) {
        super.attachView(view);
    }

    void setUserId(int userId) {
        getView().fillPosts(mModel.getPosts(userId));
    }

    void setUserName(String userName) {
        getView().setUserName(userName);
    }

    void postAdd(Post post) {
        mModel.addPost(post);
    }

    void postRemove(int postId) {
        mModel.removePost(postId);
    }

    void postClick(Post post) {
        getView().startCommentsActivity(post.getId());
    }
}