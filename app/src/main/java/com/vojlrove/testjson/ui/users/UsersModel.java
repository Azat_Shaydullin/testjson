package com.vojlrove.testjson.ui.users;

import com.vojlrove.testjson.db.Dictionaries;
import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.model.User;
import com.vojlrove.testjson.network.APIManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class UsersModel {
    private APIManager mApiManager;
    private Dictionaries mDictionaries;

    @Inject
    public UsersModel(APIManager apiManager, Dictionaries dictionaries) {
        mApiManager = apiManager;
        mDictionaries = dictionaries;
    }

    public Single<List<User>> getUsers() {
        return mApiManager.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(users -> mDictionaries.setUsers(users));
    }

    public Single<List<Post>> getPosts() {
        return mApiManager.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(posts -> {
                    for (Post post : posts) {
                        int postId = post.getId();
                        post.setNumberOfComments(mDictionaries.getComments(postId).size());
                    }
                    mDictionaries.setPosts(posts);
                });
    }

    public Single<List<Comment>> getComments() {
        return mApiManager.getComments()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(comments -> mDictionaries.setComments(comments));
    }
}