package com.vojlrove.testjson.ui.comments;

import com.arellomobile.mvp.InjectViewState;
import com.vojlrove.testjson.app.App;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.ui.BasePresenter;

import javax.inject.Inject;

@InjectViewState
public class CommentsPresenter extends BasePresenter<IComments> {

    @Inject
    CommentsModel mModel;

    CommentsPresenter() {
        App.getAppComponent().inject(this);
    }

    void setPostId(int postId) {
        getView().fillComments(mModel.getComments(postId));

        Post post = mModel.getPost(postId);
        getView().setPostBody(post.getBody());
        getView().setPostTitle(post.getTitle());
    }

}