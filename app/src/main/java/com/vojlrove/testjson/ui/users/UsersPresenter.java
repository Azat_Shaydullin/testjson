package com.vojlrove.testjson.ui.users;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.vojlrove.testjson.app.App;
import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.model.User;
import com.vojlrove.testjson.ui.BasePresenter;

import org.osmdroid.util.GeoPoint;

import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class UsersPresenter extends BasePresenter<IUsers> {

    @Inject
    UsersModel mModel;

    UsersPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void attachView(IUsers view) {
        super.attachView(view);
    }

    public void onFirstViewAttach() {
        getComments();
    }

    private void onError(Throwable t) {
        Log.e("Error Load", t.toString());
    }

    void userClickMap(User user) {
        double lat = user.getAddress().getGeo().getLat();
        double lng = user.getAddress().getGeo().getLng();
        GeoPoint geoPointOsm = new GeoPoint(lat, lng);
        getView().startMapDialog(geoPointOsm);
    }

    void userClick(User user) {
        getView().startPostsActivity(user.getId(), user.getName());
    }

    @SuppressLint("CheckResult")
    private void getComments() {
        mModel.getComments().subscribe(this::onSuccessLoadComments, this::onError);
    }

    private void onSuccessLoadComments(List<Comment> comments) {
        getPosts();
    }

    @SuppressLint("CheckResult")
    private void getPosts() {
        mModel.getPosts().subscribe(this::onSuccessLoadPosts, this::onError);
    }

    private void onSuccessLoadPosts(List<Post> posts) {
        getUsers();
    }

    @SuppressLint("CheckResult")
    private void getUsers() {
        mModel.getUsers().subscribe(this::onSuccessLoadUsers, this::onError);
    }

    private void onSuccessLoadUsers(List<User> users) {
        getView().fillUsers(users);
    }
}