package com.vojlrove.testjson.ui.posts;

import com.vojlrove.testjson.db.Dictionaries;
import com.vojlrove.testjson.model.Post;

import java.util.List;

import javax.inject.Inject;


public class PostsModel {
    private Dictionaries mDictionaries;

    @Inject
    public PostsModel(Dictionaries dictionaries) {
        mDictionaries = dictionaries;
    }

    public List<Post> getPosts(int userId) {
        return mDictionaries.getPosts(userId);
    }

    public void addPost(Post post) {
        mDictionaries.addPost(post);
    }

    public void removePost(int postId) {
        mDictionaries.removePost(postId);
    }
}