package com.vojlrove.testjson.ui.comments;

import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.ui.IBaseView;

import java.util.List;

public interface IComments extends IBaseView {
    void fillComments(List<Comment> comments);

    void setPostTitle(String postTitle);

    void setPostBody(String postBody);
}
