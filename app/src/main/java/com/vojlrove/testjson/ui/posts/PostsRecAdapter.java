package com.vojlrove.testjson.ui.posts;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.vojlrove.testjson.R;
import com.vojlrove.testjson.model.Post;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostsRecAdapter extends RecyclerView.Adapter<PostsRecAdapter.ViewHolder> {
    private List<Post> mItems;

    private PostsPresenter mPresenter;

    PostsRecAdapter(PostsPresenter presenter, List<Post> posts) {
        mPresenter = presenter;
        mItems = posts;
    }

    void setData(List<Post> data) {
        mItems = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Post currentPost = mItems.get(position);
        holder.mPostTitle.setText(currentPost.getTitle());
        int val = currentPost.getNumberOfComments();
        if (val == 1) {
            holder.mNumComments.setText(val + " comment");
        } else {
            holder.mNumComments.setText(val + " comments");
        }
        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.post_card_view)
        CardView mPostCardView;
        @BindView(R.id.post_title)
        TextView mPostTitle;
        @BindView(R.id.num_comments)
        TextView mNumComments;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final Post item) {
            itemView.setOnClickListener(v -> {
                mPresenter.postClick(item);
            });
        }
    }

    public void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Post item, int position) {
        mItems.add(position, item);
        notifyItemInserted(position);
    }

    public List<Post> getData() {
        return mItems;
    }
}