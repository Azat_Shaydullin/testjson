package com.vojlrove.testjson.ui;

import com.arellomobile.mvp.MvpPresenter;

public class BasePresenter<T extends IBaseView> extends MvpPresenter<T> {

    @Override
    public void attachView(T view) {
        super.attachView(view);
    }

    @Override
    public void detachView(T view) {
        super.detachView(view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected T getView() {
        return getViewState();
    }
}