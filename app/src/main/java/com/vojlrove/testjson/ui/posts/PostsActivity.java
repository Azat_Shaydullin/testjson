package com.vojlrove.testjson.ui.posts;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.vojlrove.testjson.R;
import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.ui.BaseActivity;
import com.vojlrove.testjson.ui.SwipeToDeleteCallback;
import com.vojlrove.testjson.ui.comments.CommentsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PostsActivity extends BaseActivity implements IPosts {
    @BindView(R.id.posts_constraint_layout)
    ConstraintLayout mPostsConstraintLayout;
    @BindView(R.id.posts_recycler_view)
    RecyclerView mPostsRecyclerView;
    @BindView(R.id.user_name)
    TextView mUserNameTextView;

    @InjectPresenter
    PostsPresenter mPresenter;

    private PostsRecAdapter mAdapter;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        unbinder = ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Posts");
        }

        mPresenter.setUserId(getIntent().getIntExtra("user_id", -1));
        mPresenter.setUserName(getIntent().getStringExtra("user_name"));

        enableSwipeToDeleteAndUndo();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void fillPosts(List<Post> posts) {
        if (mAdapter == null) {
            mPostsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mAdapter = new PostsRecAdapter(mPresenter, posts);
            mPostsRecyclerView.setAdapter(mAdapter);

        } else {
            mAdapter.setData(posts);
        }
    }

    @Override
    public void setUserName(String userName) {
        mUserNameTextView.setText(userName);
    }

    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(PostsActivity.this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                final int position = viewHolder.getAdapterPosition();
                final Post item = mAdapter.getData().get(position);

                mAdapter.removeItem(position);
                mPresenter.postRemove(item.getId());

                Snackbar snackbar = Snackbar
                        .make(mPostsConstraintLayout, "Item was removed from the list.", Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdapter.restoreItem(item, position);
                        mPostsRecyclerView.scrollToPosition(position);

                        mPresenter.postAdd(item);
                    }
                });

                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();

            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(mPostsRecyclerView);
    }

    @Override
    public void startCommentsActivity(int postId) {
        Intent intent = new Intent(PostsActivity.this, CommentsActivity.class);
        intent.putExtra("post_id", postId);
        startActivity(intent);
    }
}