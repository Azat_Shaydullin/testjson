package com.vojlrove.testjson.ui.comments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.vojlrove.testjson.R;
import com.vojlrove.testjson.model.Comment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsRecAdapter extends RecyclerView.Adapter<CommentsRecAdapter.ViewHolder> {
    private List<Comment> mItems;

    CommentsRecAdapter(List<Comment> comments) {
        mItems = comments;
    }

    void setData(List<Comment> data) {
        mItems = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Comment currentComment = mItems.get(position);
        holder.mCommentBody.setText(currentComment.getBody());

        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.comment_card_view)
        CardView mUserCardView;
        @BindView(R.id.comment_body)
        TextView mCommentBody;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final Comment item) {
//            itemView.setOnClickListener(v -> {
//                listener.onCommentClick(item);
//            });
        }
    }
}
