package com.vojlrove.testjson.ui;

import android.widget.Toast;

import com.vojlrove.testjson.androidx.CustomMvpAppCompatFragment;

public class BaseFragment extends CustomMvpAppCompatFragment {

    public void showToast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }
}