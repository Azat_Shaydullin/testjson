package com.vojlrove.testjson.ui.users;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vojlrove.testjson.R;
import com.vojlrove.testjson.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersRecAdapter extends RecyclerView.Adapter<UsersRecAdapter.ViewHolder> {
    private List<User> mItems;

    private UsersPresenter mPresenter;

    UsersRecAdapter(UsersPresenter presenter, List<User> users) {
        mPresenter = presenter;
        mItems = users;
    }

    void setData(List<User> data) {
        mItems = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User currentUser = mItems.get(position);
        holder.mUserName.setText(currentUser.getName());
        holder.mUserCompanyName.setText("Company: " + currentUser.getCompany().getName());

        String str = "Address: " + currentUser.getAddress().getZipCode() + ", " +
                currentUser.getAddress().getCity() + ", " +
                currentUser.getAddress().getStreet() + ", " +
                currentUser.getAddress().getSuite();
        holder.mUserAddress.setText(str);

        holder.mUserAddressOnMap.setOnClickListener(v -> mPresenter.userClickMap(currentUser));

        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_card_view)
        CardView mUserCardView;
        @BindView(R.id.user_name)
        TextView mUserName;
        @BindView(R.id.user_company_name)
        TextView mUserCompanyName;
        @BindView(R.id.user_address)
        TextView mUserAddress;
        @BindView(R.id.user_address_on_map)
        ConstraintLayout mUserAddressOnMap;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final User item) {
            itemView.setOnClickListener(v -> {
                mPresenter.userClick(item);
            });
        }
    }
}
