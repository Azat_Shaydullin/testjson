package com.vojlrove.testjson.ui.comments;

import com.vojlrove.testjson.db.Dictionaries;
import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.model.Post;

import java.util.List;

import javax.inject.Inject;

public class CommentsModel {
    private Dictionaries mDictionaries;

    @Inject
    public CommentsModel(Dictionaries dictionaries) {
        mDictionaries = dictionaries;
    }

    public List<Comment> getComments(int postId) {
        return mDictionaries.getComments(postId);
    }

    public Post getPost(int postId) {
        return  mDictionaries.getPost(postId);
    }
}