package com.vojlrove.testjson.ui.users;

import com.vojlrove.testjson.model.User;
import com.vojlrove.testjson.ui.IBaseView;

import org.osmdroid.util.GeoPoint;

import java.util.List;

public interface IUsers extends IBaseView {

    void fillUsers(List<User> users);

    void startPostsActivity(int userId, String userName);

    void startMapDialog(GeoPoint point);
}
