package com.vojlrove.testjson.ui.comments;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vojlrove.testjson.R;
import com.vojlrove.testjson.model.Comment;
import com.vojlrove.testjson.ui.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CommentsActivity extends BaseActivity implements IComments {

    @BindView(R.id.comments_recycler_view)
    RecyclerView mCommentsRecyclerView;
    @BindView(R.id.post_title)
    TextView mPostTitleTextView;
    @BindView(R.id.post_body)
    TextView mPostBodyTextView;

    @InjectPresenter
    CommentsPresenter mPresenter;

    private CommentsRecAdapter mAdapter;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        unbinder = ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Comments");
        }

        mPresenter.setPostId(getIntent().getIntExtra("post_id", -1));
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void fillComments(List<Comment> comments) {
        if (mAdapter == null) {
            mCommentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mAdapter = new CommentsRecAdapter(comments);
            mCommentsRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setData(comments);
        }
    }

    @Override
    public void setPostTitle(String postTitle) {
        mPostTitleTextView.setText(postTitle);
    }

    @Override
    public void setPostBody(String postBody) {
        mPostBodyTextView.setText(postBody);
    }
}