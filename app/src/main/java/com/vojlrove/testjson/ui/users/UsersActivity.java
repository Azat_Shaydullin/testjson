package com.vojlrove.testjson.ui.users;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vojlrove.testjson.BuildConfig;
import com.vojlrove.testjson.R;
import com.vojlrove.testjson.model.User;
import com.vojlrove.testjson.ui.BaseActivity;
import com.vojlrove.testjson.ui.posts.PostsActivity;

import org.osmdroid.config.Configuration;
import org.osmdroid.config.IConfigurationProvider;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static org.osmdroid.tileprovider.util.StorageUtils.getStorage;

public class UsersActivity extends BaseActivity implements IUsers {
    public static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSIONS_TO_LOAD_MAP = 0x0987;

    @BindView(R.id.users_recycler_view)
    RecyclerView mUsersRecyclerView;

    @InjectPresenter
    UsersPresenter mPresenter;

    private long mBackPressed;
    private UsersRecAdapter mAdapter;
    private Unbinder unbinder;

    private GeoPoint mPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        unbinder = ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Users");
        }

        mPresenter.onFirstViewAttach();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + 2000 > System.currentTimeMillis()) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        } else {
            this.showToast("Нажмите еще раз для выхода");
        }
        mBackPressed = System.currentTimeMillis();
    }

    @Override
    public void fillUsers(List<User> users) {
        if (mAdapter == null) {
            mUsersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mAdapter = new UsersRecAdapter(mPresenter, users);
            mUsersRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setData(users);
        }
    }

    @Override
    public void startPostsActivity(int userId, String userName) {
        Intent intent = new Intent(UsersActivity.this, PostsActivity.class);
        intent.putExtra("user_id", userId);
        intent.putExtra("user_name", userName);
        this.startActivity(intent);
    }

    @Override
    public void startMapDialog(GeoPoint point) {
        mPoint = point;
        if (ContextCompat.checkSelfPermission(UsersActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            //osmdroid не мог без этого на некоторых устройствах найти директорию для локального хранилища
            IConfigurationProvider provider = Configuration.getInstance();
            provider.setUserAgentValue(BuildConfig.APPLICATION_ID);
            provider.setOsmdroidBasePath(getStorage());
            provider.setOsmdroidTileCache(getStorage());

            Dialog dialog = new Dialog(UsersActivity.this, R.style.theme_map_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_map);

            MapView mMap = dialog.findViewById(R.id.dialog_map_view);

            Marker marker = new Marker(mMap);
            marker.setPosition(point);
            mMap.getOverlays().add(marker);
            mMap.setTileSource(TileSourceFactory.MAPNIK);
            mMap.setMultiTouchControls(true);
            mMap.setBuiltInZoomControls(false);
            mMap.getController().setZoom(4);
            mMap.getController().setCenter(point);
            dialog.show();

        } else {
            String[] INITIAL_PERMS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(UsersActivity.this,
                    INITIAL_PERMS, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSIONS_TO_LOAD_MAP);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSIONS_TO_LOAD_MAP) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i("PERMISSION", "granted");

                startMapDialog(mPoint);
            } else {
                Log.i("PERMISSION", "denied");
            }
            return;
        }
    }
}