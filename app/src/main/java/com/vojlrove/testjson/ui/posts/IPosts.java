package com.vojlrove.testjson.ui.posts;

import com.vojlrove.testjson.model.Post;
import com.vojlrove.testjson.ui.IBaseView;

import java.util.List;

public interface IPosts extends IBaseView {
    void fillPosts(List<Post> posts);

    void setUserName(String userName);

    void startCommentsActivity(int postId);
}
