package com.vojlrove.testjson.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.VALUE)
public class Company {

    @SerializedName("name")
    private String mName;

    @SerializedName("catchPhrase")
    private String mCatchPhrase;

    @SerializedName("bs")
    private String mBs;

    public String getName() {
        return mName;
    }

    public String getCatchPhrase() {
        return mCatchPhrase;
    }

    public String getBs() {
        return mBs;
    }
}
