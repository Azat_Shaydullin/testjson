package com.vojlrove.testjson.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.VALUE)
public class Post {

    @SerializedName("userId")
    private int mUserId;

    @SerializedName("id")
    private int mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("body")
    private String mBody;

    private int mNumberOfComments;

    public int getUserId() {
        return mUserId;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getBody() {
        return mBody;
    }

    public int getNumberOfComments() {
        return mNumberOfComments;
    }

    public void setNumberOfComments(int value) {
        mNumberOfComments = value;
    }
}
