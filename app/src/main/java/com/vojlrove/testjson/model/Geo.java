package com.vojlrove.testjson.model;

import com.google.gson.annotations.SerializedName;

public class Geo {

    @SerializedName("lat")
    private String mLat;

    @SerializedName("lng")
    private String mLng;

    public Double getLat() {
        return Double.parseDouble(mLat);
    }

    public Double getLng() {
        return Double.parseDouble(mLng);
    }
}

