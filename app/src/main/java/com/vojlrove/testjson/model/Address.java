package com.vojlrove.testjson.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.VALUE)
public class Address {

    @SerializedName("street")
    private String mStreet;

    @SerializedName("suite")
    private String mSuite;

    @SerializedName("city")
    private String mCity;

    @SerializedName("zipcode")
    private String mZipcode;

    @SerializedName("geo")
    private Geo mGeo;

    public String getStreet() {
        return mStreet;
    }

    public String getSuite() {
        return mSuite;
    }

    public String getCity() {
        return mCity;
    }

    public String getZipCode() {
        return mZipcode;
    }

    public Geo getGeo() {
        return mGeo;
    }
}
